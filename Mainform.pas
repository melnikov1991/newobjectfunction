unit Mainform;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,Generics.Collections, BlockLIst, block,
  hour, hourSimple, VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.Series,
  Vcl.ExtCtrls, VCLTee.TeeProcs, VCLTee.Chart, day;

type
  TForm2 = class(TForm)
    Chart1: TChart;
    Series2: TBarSeries;
    Series3: TBarSeries;
    Series4: TBarSeries;
    Series5: TBarSeries;
    Series6: TBarSeries;
    Series7: TBarSeries;
    Series8: TBarSeries;
    Series9: TLineSeries;
    Series1: TBarSeries;
    procedure FormCreate(Sender: TObject);

  private
    procedure SetHoursSimple;
    procedure PlotHourSimple(ch: TChart);
    procedure PrepareInputBlocks;
    procedure PlotHourLF(ch:Tchart);
    procedure PlotdayLF(ch: TChart);
    { Private declarations }
  public
    { Public declarations }
  end;



var
  Form2: TForm2;
  blockList:TBlockList;
  hour:Thour;

  InputBlocks:array[0..9] of Tblock;
  InputHourSimple:ThoursSimple;


  day:Tday;

implementation

{$R *.dfm}






procedure TForm2.FormCreate(Sender: TObject);
var
  res: string;
begin

PrepareInputBlocks;
SetHoursSimple;


PlotHourSimple(chart1);


day.SetBlockList(blockList);
day.SetPowerHours(InputHourSimple);
day.Load;

showmessage(day.BlockList.ToListSpinRes);

PlotdayLF(chart1);

//showmessage(floattostr(day.LoadFactor));
//showmessage(floattostr(day.Energy)) ;
//showmessage(floattostr(day.BlockList.GetBlockFromName('���(���)').FuelTotalPrice)) ;

///
end;

procedure TForm2.SetHoursSimple;
var
  I: Integer;
begin

{$REGION 'LoadData'}
  InputHourSimple[0]:=  3925;
 InputHourSimple[1]:=  3863;
 InputHourSimple[2]:=  3794;
 InputHourSimple[3]:=  3891;
 InputHourSimple[4]:=  3828;
 InputHourSimple[5]:=  4148;
 InputHourSimple[6]:=  4841;             // 4841;
 InputHourSimple[7]:=  5378;
 InputHourSimple[8]:=  5631;
 InputHourSimple[9]:=  5726;
 InputHourSimple[10]:= 5532;
 InputHourSimple[11]:= 5422;
 InputHourSimple[12]:= 5451;
 InputHourSimple[13]:= 5419;
 InputHourSimple[14]:= 5314;
 InputHourSimple[15]:= 5272;
 InputHourSimple[16]:= 5168;
 InputHourSimple[17]:= 5518;
 InputHourSimple[18]:= 5680;
 InputHourSimple[19]:= 5523;
 InputHourSimple[20]:= 5369;
 InputHourSimple[21]:= 5055;
 InputHourSimple[22]:= 4577;
 InputHourSimple[23]:= 4256;


 for I := 0 to 23 do
  InputHourSimple[i]:=InputHourSimple[i]+1400;


{$ENDREGION}



end;

procedure TForm2.PlotHourSimple(ch: TChart);
var
  I: Integer;
begin

  ch.SeriesList[8].Color:=clblue;
  ch.SeriesList[8].Title:='�������� ��������';
  for I := 0 to high(InputHourSimple) do
    ch.Series[8].AddXY(i,inputHourSimple[i]);

end;

procedure TForm2.PrepareInputBlocks;
var
  I: Integer;
begin

// 8 ������
            {$REGION 'BlockData'}
InputBlocks[1].name:='���';
InputBlocks[1].FixPow:=480;
InputBlocks[1].VarPow:=520;
InputBlocks[1].energyFunc.Qx:=180;
InputBlocks[1].energyFunc.r:=2.516;
InputBlocks[1].fuelName:='������� �������';
InputBlocks[1].fuelPrice:=30;
InputBlocks[1].fixCost:=52000;
InputBlocks[1].varCost:=0.34;
InputBlocks[1].spinResSort:=0;   // � �����            !!!
InputBlocks[1].color:=clred;
InputBlocks[1].isNpp:=true;
InputBlocks[1].isChp:=false;


InputBlocks[0].name:='���(�)';
InputBlocks[0].FixPow:=2735;
InputBlocks[0].VarPow:=550;
InputBlocks[0].energyFunc.Qx:=5;
InputBlocks[0].energyFunc.r:=0.5;
InputBlocks[0].fuelName:='���';
InputBlocks[0].fuelPrice:=45;
InputBlocks[0].fixCost:=32830;
InputBlocks[0].varCost:=1.79;
InputBlocks[0].spinResSort:=1;   // � �����          !!!
InputBlocks[0].color:=clgreen;
InputBlocks[0].isNpp:=false;
InputBlocks[0].isChp:=true;


InputBlocks[2].name:='���(�)';
InputBlocks[2].FixPow:=219;
InputBlocks[2].VarPow:=0;
InputBlocks[2].energyFunc.Qx:=5;
InputBlocks[2].energyFunc.r:=0.5;
InputBlocks[2].fuelName:='���';
InputBlocks[2].fuelPrice:=45;
InputBlocks[2].fixCost:=20000;
InputBlocks[2].varCost:=2.5;
InputBlocks[2].spinResSort:=0;   // � �����       !!!
InputBlocks[2].color:=clyellow;
InputBlocks[2].isNpp:=false;
InputBlocks[2].isChp:=true;


InputBlocks[3].name:='����-���';
InputBlocks[3].FixPow:=171;
InputBlocks[3].VarPow:=0;
InputBlocks[3].energyFunc.Qx:=5;
InputBlocks[3].energyFunc.r:=0.5;
InputBlocks[3].fuelName:='���';
InputBlocks[3].fuelPrice:=45;
InputBlocks[3].fixCost:=20000;
InputBlocks[3].varCost:=2.5;
InputBlocks[3].spinResSort:=0;   // � �����       !!!
InputBlocks[3].color:=clgray;
InputBlocks[3].isNpp:=false;
InputBlocks[3].isChp:=true;


InputBlocks[4].name:='���(���)';
InputBlocks[4].FixPow:=140;
InputBlocks[4].VarPow:=1447;
InputBlocks[4].energyFunc.Qx:=5;
InputBlocks[4].energyFunc.r:=0.5;
InputBlocks[4].fuelName:='���';
InputBlocks[4].fuelPrice:=45;
InputBlocks[4].fixCost:=20000;
InputBlocks[4].varCost:=2.5;
InputBlocks[4].spinResSort:=2;   // � �����       !!!
InputBlocks[4].color:=clAqua;
InputBlocks[4].isNpp:=false;
InputBlocks[4].isChp:=false;

InputBlocks[5].name:='���(�����)';
InputBlocks[5].FixPow:=400;
InputBlocks[5].VarPow:=0;
InputBlocks[5].energyFunc.Qx:=5;
InputBlocks[5].energyFunc.r:=0.5;
InputBlocks[5].fuelName:='�����';
InputBlocks[5].fuelPrice:=60;
InputBlocks[5].fixCost:=20000;
InputBlocks[5].varCost:=2.5;
InputBlocks[5].spinResSort:=0;   // � �����       !!!
InputBlocks[5].color:=clblack;
InputBlocks[5].isNpp:=false;
InputBlocks[5].isChp:=false;

InputBlocks[6].name:='����-�������';
InputBlocks[6].FixPow:=318;
InputBlocks[6].VarPow:=0;
InputBlocks[6].energyFunc.Qx:=544;
InputBlocks[6].energyFunc.r:=0.9;
InputBlocks[6].fuelName:='���';
InputBlocks[6].fuelPrice:=45;
InputBlocks[6].fixCost:=20000;
InputBlocks[6].varCost:=2.5;
InputBlocks[6].spinResSort:=0;   // � �����       !!!
InputBlocks[6].color:=clFuchsia;
InputBlocks[6].isNpp:=false;
InputBlocks[6].isChp:=false;


InputBlocks[7].name:='���';
InputBlocks[7].FixPow:=0;
InputBlocks[7].VarPow:=500;
InputBlocks[7].energyFunc.Qx:=5;
InputBlocks[7].energyFunc.r:=0.5;
InputBlocks[7].fuelName:='���';
InputBlocks[7].fuelPrice:=45;
InputBlocks[7].fixCost:=20000;
InputBlocks[7].varCost:=2.5;
InputBlocks[7].spinResSort:=3;   // � �����       !!!
InputBlocks[7].color:=clPurple;
InputBlocks[7].isNpp:=false;
InputBlocks[7].isChp:=false;
            {$ENDREGION}



for I := 0 to 7 do
    blockList.AddBlock(InputBlocks[i]);


///
end;

procedure TForm2.PlotHourLF(ch:tchart);
var
  I: Integer;
begin
  for I := 0 to hour.BlockList.BlockCount - 1 do
  begin
    ch.SeriesList[i].Color := hour.BlockList.fblocks[i].color;
    ch.SeriesList[i].AddXY(0, hour.BlockList.fblocks[i].TotalCurrentPower);
  end;
end;

procedure TForm2.PlotdayLF(ch:tchart);
var
  I: Integer;
  j: Integer;
begin


  for I := 0 to HoursPerDay - 1 do
  begin
    for j := 0 to day.BlockList.BlockCount - 1 do
    begin
     ch.SeriesList[j].Title:=day.BlockList.fblocks[j].name;

     ch.SeriesList[j].Color:=day.BlockList.fblocks[j].color;

     ch.SeriesList[j].AddXY(i,day.Hours[i].BlockList[j].TotalCurrentPower);


    end;
  end;


end;



end.

