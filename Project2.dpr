program Project2;

uses
  Vcl.Forms,
  Mainform in 'Mainform.pas' {Form2},
  hour in 'hour.pas',
  BlockLIst in 'BlockLIst.pas',
  block in 'block.pas',
  day in 'day.pas',
  groupDays in 'groupDays.pas',
  groupLIst in 'groupLIst.pas',
  groupSelected in 'groupSelected.pas',
  groupArray in 'groupArray.pas',
  tags in 'tags.pas',
  hours in 'hours.pas',
  hourSimple in 'hourSimple.pas',
  fuelInfo in 'fuelInfo.pas',
  fuelInfoList in 'fuelInfoList.pas',
  Constants in 'Constants.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
