unit block;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Generics.Collections,Vcl.Graphics;

type

TenergyFunc = record
Qx,r:real;
end;

Tblock = record

public

{ TODO : ����. ���� � �����. ���� }
  name:string[50];
  FixPow:real;
  VarPow:real;
  energyFunc:TenergyFunc;
  fuelName:string[10];
  fuelPrice:real;
  fixCost:real;     //  � ����./���*���    { TODO : �����. ��������� ����� }
  varCost:real;     //  � ����./���*�
  spinResSort:integer;

  color:tcolor;
  currFixPow:real;
  currVarPow:real;

  isNpp:boolean;
  isChp:boolean;
//  isAccum:boolean;




private
    fkpdSort:double;
    fhoursCount:integer;
    function GetLoadFactor:real;
    function GetEnergy: real;
    function GetFixTotalPrice: real;
    function GetFuelMass: real;
    function GetFuelTotalPrice: real;
    function GetVarTotalPrice: real;
    function GetTotalPower: real;
    function GetTotalCurrentPower: real;
    function GetHeatRate: real;
    function GetObjFuncValue: real;
    function GetKpdSort: real;
    function GetEfficencyNominal: real;

public

 property CurrentHeatRate: real read GetHeatRate;
 property TotalPower:real read GetTotalPower;
 property LoadFactor:real read GetLoadFactor;
 property Energy: real read GetEnergy;
 property FuelMass: real read GetFuelMass;
 property FuelTotalPrice: real read GetFuelTotalPrice;
 property VarTotalPrice: real read GetVarTotalPrice;
 property FixTotalPrice: real read GetFixTotalPrice;
 property TotalCurrentPower: real read GetTotalCurrentPower;
 property ObjectFuncValue: real read GetObjFuncValue;
 property HoursCount: integer read fhoursCount write fhoursCount;
 property EfficiencyNominal: real read GetEfficencyNominal;

 property PriceKwth: real read GetKpdSort;


end;



implementation


function Tblock.GetEfficencyNominal: real;
begin
result:=TotalPower/CurrentHeatRate;

end;

function Tblock.GetEnergy: real;
begin

//result:=LoadFactor*TotalPower*hoursCount;

result:=TotalCurrentPower*fhoursCount;

end;


function Tblock.GetFuelMass: real;
begin

result:=CurrentHeatRate*0.123*fhoursCount;

end;

function Tblock.GetFuelTotalPrice: real;
begin

result:=FuelMass*fuelPrice;

end;

function Tblock.GetHeatRate: real;
begin

result:=energyFunc.Qx+energyFunc.r*TotalCurrentPower;

end;

function Tblock.GetKpdSort: real;
begin


result:=(0.123*fuelPrice)/EfficiencyNominal;

end;

function Tblock.GetLoadFactor:real;
begin

result:=Energy/(TotalPower*fhoursCount);

end;

function Tblock.GetObjFuncValue: real;
begin

result:=FuelTotalPrice+VarTotalPrice+FixTotalPrice;

end;

function Tblock.GetTotalCurrentPower: real;
begin

result:=currFixPow+currVarPow;

end;

function Tblock.GetTotalPower: real;
begin

result:=FixPow+VarPow;

end;



function Tblock.GetFixTotalPrice: real;
begin
 { TODO : 8760? }
result:=fixCost*TotalPower*fhoursCount/8760;

end;

function Tblock.GetVarTotalPrice: real;
begin
result:=Energy*varCost;
end;

end.
