unit fuelInfo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Generics.Collections;

type

TfuelInfo = record

fuelName:string[10];
fuelPrice:real;
fuelMass:real;
fuelFraction:real;
fuelTotalPrice:real;

end;


implementation

end.
