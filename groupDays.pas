unit groupDays;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Generics.Collections, hourSimple, BlockLIst, day;

type

TGroupdays = record

public
 procedure SetInputData(hoursSimple:ThoursSimple;
 countDay:integer;pBlockList:TblockList);
 procedure SetPowerHours(hoursSimple:ThoursSimple);
 procedure SetDayCount(dayCount:integer);
 procedure SetBlockList(blockList:TblockList);
 procedure Load;

//��� ����� �������� ��������
//��������� ����������� �������

private
 fname:string;
 fday:Tday;
 fdayCount:integer;
 fblockList:TblockList;
 ftag: string;
 function GetEnergy: real;
 function GetLoadFactor: real;
 function GetMaxPower: real;
 function GetMinPower: real;
 function GetTotalHours: integer;
 function GetFixTotalPrice: real;
 function GetFuelMass: real;
 function GetFuelTotalPrice: real;
 function GetVarTotalPrice: real;
 function GetObjFuncValue: real;
 // ���������� ���������� �� ��������� ����� ���������� ����
public

 property GroupName: string read fname write fname;
 property MinPower: real read GetMinPower;
 property MaxPower: real read GetMaxPower;
 property Energy: real read GetEnergy;
 property LoadFactor: real read GetLoadFactor;
 property HoursCount: integer read GetTotalHours;
 property FuelMass: real read GetFuelMass;
 property FuelTotalPrice: real read GetFuelTotalPrice;
 property VarTotalPrice: real read GetVarTotalPrice;
 property FixTotalPrice: real read GetFixTotalPrice;
 property ObjectFuncValue: real read GetObjFuncValue;
 property Tag: string read ftag write ftag;
 property DayCount: integer read fdaycount;
 property Day: Tday read fday;

// ���������� �� ������ � �������� ������
 property BlockList:TblockList read fblockList;

end;


implementation

function TGroupdays.GetEnergy:real;
var
  i: Integer;
begin

if fdayCount<>0 then
  begin

    result:=0;
    for i:= 0 to fdayCount-1 do
     begin

         result:=result+fdayCount*fday.Energy;

     end;

  end
   else
     raise Exception.Create('�� ������ ���� ����������� �����');

end;

function TGroupdays.GetFixTotalPrice: real;
var
  I: Integer;
begin

result:=fdayCount*fday.FixTotalPrice

end;

function TGroupdays.GetFuelMass: real;
begin

result:=fdayCount*fday.FuelMass;

end;

function TGroupdays.GetFuelTotalPrice: real;
begin

result:=fdayCount*FuelTotalPrice;


end;

function TGroupdays.GetLoadFactor: real;
begin

result:=Energy/(HoursCount*MaxPower);

end;

function TGroupdays.GetMaxPower: real;
var
  i: Integer;
begin

if fdayCount<>0 then
      result:=fday.MaxPower
    else
     raise  Exception.Create('��������� �����');


end;

function TGroupdays.GetMinPower: real;
var
  i: Integer;
begin

if fdayCount<>0 then
    result:=fday.MinPower
    else
     raise Exception.Create('��������� �����');

end;

function TGroupdays.GetObjFuncValue: real;
begin

result:=(VarTotalPrice+FixTotalPrice+FuelTotalPrice)*fdayCount;


end;

function TGroupdays.GetTotalHours: integer;
var
  i: Integer;
begin

if fdayCount<>0 then
   result:=fdayCount*HoursPerDay
 else
  raise Exception.Create('�� ������ ���� ����������� �����');

end;

function TGroupdays.GetVarTotalPrice: real;
begin

result:=fdayCount*fday.VarTotalPrice;

end;

procedure TGroupdays.Load;
var
  I: Integer;
begin

self.fday.Load;




for I := 0 to fblockList.blockCount-1 do
  begin
   fblockList.fblocks[i].currFixPow:=fday.BlockList[i].currFixPow;
   fblockList.fblocks[i].currVarPow:=fday.BlockList[i].currVarPow;
  end;


end;


procedure TGroupdays.SetInputData(hoursSimple:ThoursSimple;
countDay:integer;pBlockList:TblockList);
begin

self.fdayCount:=countDay;
self.fday.SetPowerHours(hoursSimple);
self.fday.SetBlockList(pBlockList);


self.fblockList:=pBlockList;
self.fblockList.SetHoursCount(HoursCount);


end;

procedure TGroupdays.SetBlockList(blockList: TblockList);
begin

self.fblockList:=blockList;
self.fday.SetBlockList(blockList);

self.fblockList.SetHoursCount(fdayCount*HoursPerDay);

end;

procedure TGroupdays.SetDayCount(dayCount: integer);
begin

self.fdayCount:=dayCount;

end;

procedure TGroupdays.SetPowerHours(hoursSimple: ThoursSimple);
begin

fday.SetPowerHours(hoursSimple);


end;

end.
