unit groupSelected;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Generics.Collections, groupLIst, tags, block, hour, BlockLIst,
  groupArray, groupDays;

type

TGroupListSelected = record

fGArrayOrig:TGroupList;
fGArraySlct:TGroupList;

procedure SetOrigGroupList(pGArrayOrig:TGroupList);
procedure ToOriginList;
procedure AddByTag(tag:string);
procedure AddByName(name:string);
procedure RemoveByTag(tag:string);
procedure RemoveByName(groupName:string);

procedure ClearSlcArray;

private
  function GetSelectedTags:Ttags;
  procedure UpdateSelectedData;  //!
  function GetBlock(index: integer): Tblock;
  function GetEnergy: real;
  function GetFixTotalPrice: real;
  function GetFuelMass: real;
  function GetFuelTotalPrice: real;
  function GetObjFuncValue: real;
  function GetTotalDays: integer;
  function GetTotalHours: integer;
  function GetVarTotalPrice: real;


 property Stags: Ttags read GetSelectedTags;
 property SfuelMass: real read GetFuelMass;
 property SfuelTotalPrice: real read GetFuelTotalPrice;
 property Senergy: real read GetEnergy;
 property SvarTotalPrice: real read GetVarTotalPrice;
 property SfixTotalPrice: real read GetFixTotalPrice;
 property ShoursCount: integer read GetTotalHours;
 property StotalDays: integer read GetTotalDays;
 property SobjectFuncValue: real read GetObjFuncValue;
 property Sblocks[index:integer]: Tblock read GetBlock;



end;

implementation

procedure TGroupListSelected.AddByName(name: string);
var
  I: Integer;
  index: Integer;
begin

if fGArraySlct.HasName(name,index) or (name='')  then
       begin
//        showmessage('��� '+name+' ��� ���� � ������');
        exit;
       end;

if not fGArrayOrig.HasName(name,index) then
       begin
//        showmessage('��� '+name+' ��� ���� � ������');
        exit;
       end;


for I := 0 to fGArrayOrig.fGArray.groupCount-1 do
       if fGArrayOrig.fGArray.gArray[i].GroupName=name then
         begin
          fGArraySlct.AddGroupDays(fGArrayOrig.fGArray.gArray[i]);
          exit;
         end;

end;

procedure TGroupListSelected.AddByTag(tag: string);
var
  I: Integer;
begin

// ���� ���� ������� � �����, �� ����� �������� ����������

for I := 0 to fGArrayOrig.fGArray.groupCount-1 do
 if fGArrayOrig.fGArray.gArray[i].Tag=tag then
    begin
     fGArraySlct.AddGroupDays(fGArrayOrig.fGArray.gArray[i]);
//     exit;
    end;


end;


function TGroupListSelected.GetBlock(index: integer): Tblock;
begin

result:=Self.fGArraySlct.Blocks[index];

end;

function TGroupListSelected.GetEnergy: real;
begin

result:=Self.fGArraySlct.Energy;

end;

function TGroupListSelected.GetFixTotalPrice: real;
begin

result:=Self.fGArraySlct.FixTotalPrice;

end;

function TGroupListSelected.GetFuelMass: real;
begin

result:=Self.fGArraySlct.FuelMass;

end;

function TGroupListSelected.GetFuelTotalPrice: real;
begin

result:=Self.fGArraySlct.FuelTotalPrice;

end;

function TGroupListSelected.GetObjFuncValue: real;
begin

result:=Self.fGArraySlct.ObjectFuncValue;

end;

function TGroupListSelected.GetSelectedTags: Ttags;
begin

result:=fGArraySlct.ftags;

end;

function TGroupListSelected.GetTotalDays: integer;
begin

result:=Self.fGArraySlct.TotalDays;

end;

function TGroupListSelected.GetTotalHours: integer;
begin

result:=Self.fGArraySlct.HoursCount;

end;

function TGroupListSelected.GetVarTotalPrice: real;
begin

result:=Self.fGArraySlct.VarTotalPrice;

end;

procedure TGroupListSelected.ClearSlcArray;
var
  I: Integer;
  emptyGroupArray:TgroupArray ;
begin

  self.fGArraySlct.fGArray:=emptyGroupArray;

//  self.Stags.ClearTags;

end;

procedure TGroupListSelected.RemoveByName(groupName: string);
var
  I: Integer;
  emptyGroupDays: TgroupDays;
begin

for I := 0 to fGArraySlct.fGArray.groupCount-1 do
   if fGArraySlct.fGArray.gArray[i].GroupName=groupName then
       fGArraySlct.fGArray.gArray[i]:=emptyGroupDays;

UpdateSelectedData;

end;

procedure TGroupListSelected.RemoveByTag(tag: string);
var
  I: Integer;
  emptyGroupDays: TgroupDays;
begin

for I := 0 to fGArraySlct.fGArray.groupCount-1 do
 if fGArraySlct.fGArray.gArray[i].Tag=tag then
     fGArraySlct.fGArray.gArray[i]:=emptyGroupDays;

UpdateSelectedData;

end;

procedure TGroupListSelected.SetOrigGroupList(pGArrayOrig:TGroupList);
begin

Self.fGArrayOrig:=pGArrayOrig;



end;

procedure TGroupListSelected.ToOriginList;
begin
 fGArraySlct:=fGArrayOrig;
end;

procedure TGroupListSelected.UpdateSelectedData;
var
  BufArray: TGroupArray;
  I: Integer;

begin

BufArray:=fGArraySlct.fGArray;

self.ClearSlcArray;

   { TODO : ��������� }

for I := 0 to BufArray.groupCount-1 do
  if BufArray.gArray[i].GroupName<>'' then
     fGArraySlct.AddGroupDays(BufArray.gArray[i]);


//fGArraySlct.fGArray.
//
end;

end.
