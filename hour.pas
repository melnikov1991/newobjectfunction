unit hour;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Generics.Collections, BlockLIst, fuelInfoList;

type

Thour = record

public

 procedure SetInputData(const powerValue:real;BufblockList:TblockList);
 procedure Load;

 procedure SetBlockList(BufblockList:TblockList);


// ���������� ��������� ����������� ������� (��� - �.�.� - ������� - ������)

private
    fblockList:TblockList;
    fpowerValue:real;
    function GetEnergy: real;
    function GetFixTotalPrice: real;
    function GetFuelMass: real;
    function GetFuelTotalPrice: real;
    function GetVarTotalPrice: real;
    function GetFuelInfoList: TfuelInfoList;
    function GetObjFuncValue: real;
    procedure SetPowerValue(const powerValue: real);

// ���������� ���������� �� ���

public

  property Energy: real read GetEnergy;
  property FuelMass: real read GetFuelMass;
  property FuelTotalPrice: real read GetFuelTotalPrice;
  property VarTotalPrice: real read GetVarTotalPrice;
  property FixTotalPrice: real read GetFixTotalPrice;
  property FuelInfoList: TfuelInfoList read GetFuelInfoList;
  property ObjectFuncValue: real read GetObjFuncValue;


  // ���������� �� ������ � �������� ����
  property BlockList: TBlockList read fblockList write fblockList;
  property PowerValue: real read fpowerValue  write SetPowerValue;


end;


implementation


function Thour.GetEnergy: real;
var
  i: Integer;
begin

result:=0;

for i := 0 to BlockList.BlockCount-1 do
  result:=result+BlockList[i].Energy;

end;



function Thour.GetFixTotalPrice: real;
var
  I: Integer;
begin

//showmessage('');

result:=0;
for I:=0 to fblockList.blockCount-1 do
 result:=result+BlockList[i].FixTotalPrice;

end;

function Thour.GetVarTotalPrice: real;
var
  I: Integer;
begin

result:=0;
for I:=0 to fblockList.blockCount-1 do
 result:=result+BlockList[i].VarTotalPrice;

end;


function Thour.GetFuelInfoList: TfuelInfoList;
var
  I: Integer;
  j: integer;
begin

with fblockList do
 begin

 // �������� ������ ����� �������
  for I := 0 to blockCount-1 do
       if not result.HasFuelType(BlockList[i].fuelName) then
              Result.AddFuel(BlockList[i].fuelName,BlockList[i].fuelPrice);


 // ������. ���� ������� �� �����
  for I := 0 to blockCount-1 do
    for j := 0 to Result.fuelCount-1 do
         if BlockList[i].fuelName=result.fuel[j].fuelName then
             Result.fuel[j].fuelMass:=Result.fuel[j].fuelMass+BlockList[i].FuelMass;

 // ���������� ���� ���� ������� � ���������
 for j := 0 to result.fuelCount-1 do
      result.fuel[j].fuelFraction:= result.fuel[j].fuelMass/FuelMass;


 // ���������� ������ ��� ������� ���� �������
 for j := 0 to result.fuelCount-1 do
      result.fuel[j].fuelTotalPrice:=
      result.fuel[j].fuelMass*result.fuel[j].fuelPrice ;



 end;



//FuelInfo.


end;

function Thour.GetFuelMass: real;
var
  I: Integer;
begin

result:=0;
for I:=0 to fblockList.blockCount-1 do
 result:=result+BlockList[i].FuelMass;

end;

function Thour.GetFuelTotalPrice: real;
var
  I: Integer;
begin

result:=0;
for I:=0 to fblockList.blockCount-1 do
 result:=result+BlockList[i].FuelTotalPrice;

end;


function Thour.GetObjFuncValue: real;
begin

result:=FuelTotalPrice+FixTotalPrice+VarTotalPrice;

end;

procedure Thour.Load;
var
  installPower:real;
  currentPower:real;
  deficiency: string;
  i: Integer;
  deltaPow: Real;
  bPos: integer;
begin

//fblockList.SetHoursCount(1);

installPower:=fblockList.InstallPower;

if installPower<fpowerValue then
    begin
     deficiency:=floattostr(fpowerValue-installPower);
      raise Exception.Create('������������ �������� ��� ���������� ���� ����� -'+deficiency);
    end;

if installPower>=fpowerValue then
    begin
     currentPower:=installPower;
     for i:= 0 to fblockList.blockCount-1 do
       begin



        fblockList.fblocks[i].currFixPow:=BlockList[i].FixPow;
        fblockList.fblocks[i].currVarPow:=BlockList[i].VarPow;
       end;
//   ���� ���� ����� ������ ����� � ���������� ��������� ������������




     for i:=0 to fblockList.fblockCount-1 do
       begin

          bPos:=fblockList.fspinResIndex[i];

          currentPower:=currentPower-BlockList[bPos].currVarPow;
          fblockList.fblocks[bPos].currVarPow:=0;

         if currentPower<fpowerValue then
           begin
//            ���� ����� ���� �� �������� ������� ������� �� ���. �����. ��������
             deltaPow:=fpowerValue-currentPower;
             fblockList.fblocks[bPos].currVarPow:=deltaPow;
             Exit();
           end;

       end;

//      ����� ���� ������ ������� ���������� �������� ����� ����

     for i:=fblockList.blockCount-1 downto 0 do
       begin
        currentPower:=currentPower-BlockList[i].currFixPow;
        fblockList.fblocks[i].currFixPow:=0;

        if currentPower<fpowerValue then
           begin
            deltaPow:=fpowerValue-currentPower;
            fblockList.fblocks[i].currFixPow:=deltaPow;
            Exit();
           end;

       end;

    end;

end;

procedure Thour.SetBlockList(BufblockList: TblockList);
begin

//fblockList.Clear;

fblockList:=BufblockList;

fblockList.SetHoursCount(1);


end;

procedure Thour.SetInputData(const powerValue: real; BufblockList: TblockList);
begin

SetPowerValue(powerValue);
SetBlockList(BufblockList);

end;

procedure Thour.SetPowerValue(const powerValue: real);
begin

if powerValue>0 then
     fpowerValue := powerValue
    else
     raise Exception.Create('�������� � ���� ����� �� ����� �������������')
end;


end.
